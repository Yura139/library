$(function () {
  let getCards = JSON.parse(localStorage.getItem("cards"));
  // show list
  showCards(getCards);
  function showCards(data) {
    data.length == 0
      ? $("#cards-list").empty()
      : data.map((item, i) => {
          let getUsers = JSON.parse(localStorage.getItem("users"));
          let getBooks = JSON.parse(localStorage.getItem("books"));
          let visitor = getUsers.find((name) => name.id == item.idUser);
          let book = getBooks.find((name) => name.id == item.idBook);
          let dateEnd = ``;
          if (item.dateEnd == "") {
            dateEnd = `<td class="set-date" data-id="${item.id}"></td>`;
          } else {
            dateEnd = `<td>${item.dateEnd}</td>`;
          }
          $("#cards-list").append(`
          <tr>
          <td>${item.id}</td>
          <td>${visitor.name}</td>
          <td>${book.nameBook}</td>
          <td>${item.dateStart}</td>
          ${dateEnd}
          </tr>
    `);
        });
  }
  // form

  function myForm(data) {
    $(".modal form").empty();
    let visitor = "";
    let book = "";
    let getUsers = JSON.parse(localStorage.getItem("users"));
    getUsers.map((item, i) => {
      visitor += `<option value="${item.id}">${item.name}</option>`;
    });

    let getBooks = JSON.parse(localStorage.getItem("books"));
    getBooks.map((item, i) => {
      if (item.countBook > 0) {
        book += `<option value="${item.id}">${item.nameBook}</option>`;
      }
    });

    $(".modal form").append(`
      <div class="item">
      <label for="visitor">Visitor: </label>
      <select name="visitor" id="visitor">
      ${visitor}
    </select>
      </div>
      <div class="item">
      <label for="booksList">Books: </label>
      <select name="booksList" id="booksList">
      ${book}
    </select>
      </div>
      <div class="btn save" >Save</div>
    `);
  }

  //  Modal
  $(".close").click(() => {
    $(".modal").hide("fast");
  });

  // add card
  $(".new-card").click(() => {
    $(".modal h2").text("Add card");
    myForm();
    $(".modal").show("fast");
    $(".save").click(addCard);
  });

  function addCard() {
    let date = new Date().toLocaleDateString("en-US");
    let idCard = getCards.length + 1;
    let newObj = {
      id: idCard,
      idUser: parseInt($("#visitor").val()),
      idBook: parseInt($("#booksList").val()),
      dateStart: date,
      dateEnd: "",
    };

    let getBooks = JSON.parse(localStorage.getItem("books"));
    let thisBook = getBooks.find((item) => item.id == $("#booksList").val());
    let newObjBook = {
      id: thisBook.id,
      nameBook: thisBook.nameBook,
      author: thisBook.author,
      year: thisBook.year,
      namePublishing: thisBook.namePublishing,
      countPage: thisBook.countPage,
      countBook: thisBook.countBook - 1,
    };
    getBooks.splice(thisBook.id - 1, 1, newObjBook);
    booksJSON = JSON.stringify(getBooks);
    localStorage.setItem("books", booksJSON);

    getCards = JSON.parse(localStorage.getItem("cards"));
    getCards.push(newObj);
    cardsJSON = JSON.stringify(getCards);
    localStorage.setItem("cards", cardsJSON);
    $("#cards-list").empty();
    showCards(getCards);
    $(".modal").hide("fast");

    $(".set-date").click(setTime);
  }

  // add Date

  function setTime() {
    let id = $(this).data("id");
    let date = new Date().toLocaleDateString("en-US");
    let getBooks = JSON.parse(localStorage.getItem("books"));

    getCards = JSON.parse(localStorage.getItem("cards"));
    let myCard = getCards.find((item) => item.id == id);
    let thisBook = getBooks.find((item) => item.id == myCard.idBook);

    let newObj = {
      id: myCard.id,
      idUser: myCard.idUser,
      idBook: myCard.idBook,
      dateStart: myCard.dateStart,
      dateEnd: date,
    };

    let newObjBook = {
      id: thisBook.id,
      nameBook: thisBook.nameBook,
      author: thisBook.author,
      year: thisBook.year,
      namePublishing: thisBook.namePublishing,
      countPage: thisBook.countPage,
      countBook: thisBook.countBook + 1,
    };

    getBooks.splice(myCard.idBook - 1, 1, newObjBook);
    booksJSON = JSON.stringify(getBooks);
    localStorage.setItem("books", booksJSON);

    getCards.splice(myCard.id - 1, 1, newObj);
    cardsJSON = JSON.stringify(getCards);
    localStorage.setItem("cards", cardsJSON);
    $("#cards-list").empty();
    showCards(getCards);

    $(".set-date").click(setTime);
  }
  $(".set-date").click(setTime);

  // SortCard
  $(".sort").click(() => {
    getCards = JSON.parse(localStorage.getItem("cards"));

    if ($("#sort").val() == 1) {
      getCards.sort((a, b) => a.id - b.id);
    } else if ($("#sort").val() == 2) {
      getCards.sort((a, b) => {
        let aDate = new Date(a.dateEnd);
        let bDate = new Date(b.dateEnd);
        let i = 0;
        if (aDate > bDate) {
          i = 1;
        } else if (aDate < bDate) {
          i = -1;
        }
        return i;
      });
    }

    cardsJSON = JSON.stringify(getCards);
    localStorage.setItem("cards", cardsJSON);
    $("#cards-list").empty();
    showCards(getCards);

    $(".set-date").click(setTime);
  });

  //Search

  $(".search").click(() => {
    getCards = JSON.parse(localStorage.getItem("cards"));

    let filterCards = getCards.filter((el) => {
      console.log(el);
      if ($("#search").val() == "") {
        return el;
      } else {
        return el.dateStart.search($("#search").val()) != -1;
      }
    });
    $("#cards-list").empty();
    showCards(filterCards);

    $(".set-date").click(setTime);
  });
  //
  //
  //
});
