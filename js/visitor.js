$(function () {
  let getUsers = JSON.parse(localStorage.getItem("users"));

  // show list
  showUsers(getUsers);
  function showUsers(users) {
    users.length == 0
      ? $("#visitors-list").empty()
      : users.map((item, i) => {
          $("#visitors-list").append(`
          <tr>
          <td>${item.id}</td>
          <td>${item.name}</td>
          <td>${item.phone}</td>
          <td class="edit" data-id="${i}"></td>
          </tr>
    `);
        });
    $(".table tr").on("click", ".edit", editUser);
  }
  // form
  function myForm(data) {
    let myClass = "";
    if (!data) {
      data = {
        name: "",
        phone: "",
      };
      myClass = "add";
    } else {
      myClass = "accept";
    }
    $(".modal form").empty();
    $(".modal form").append(`
      <div class="item">
        <label for="name">Name: </label>
        <input type="text" id="name" value="${data.name}" />
      </div>
      <div class="item">
        <label for="phone">Phone: </label>
        <input type="text" id="phone" value="${data.phone}" />
      </div>
      <div class="btn ${myClass}" data-id="${data.id}">${myClass}</div>
    `);

    $("#phone").mask("(380) 00-00-000");
  }

  //  Modal
  $(".close").click(() => {
    $(".modal").hide("fast");
  });

  // add user
  $(".new-user").click(() => {
    $(".modal h2").text("Add user");
    myForm();
    $(".modal").show("fast");
    $(".add").click(addUser);
  });

  function addUser() {
    let required = true;
    let idUser = getUsers.length + 1;
    let newObj = {
      id: idUser,
      name: $("#name").val(),
      phone: $("#phone").val(),
    };

    $(".modal .item input").map((i, item) => {
      if ($(item).val() == "") {
        required = false;
        $(item).css({ border: "1px solid #f71f1f" });
      } else {
        $(item).css({ border: "1px solid #4baf3d" });
      }
    });
    if (!required) {
      $(".error").remove();
      $(".modal form").prepend('<div class="error">Заповніть всі поля</div>');
    } else {
      $(".error").remove();
      getUsers = JSON.parse(localStorage.getItem("users"));
      getUsers.push(newObj);
      usersJSON = JSON.stringify(getUsers);
      localStorage.setItem("users", usersJSON);
      $("#visitors-list").empty();
      showUsers(getUsers);
      $(".modal").hide("fast");
    }
  }

  //edit
  function editUser() {
    let id = $(this).data("id");
    let ourElement = getUsers[id];
    $(".modal h2").text("Edit user");
    myForm(ourElement);
    $(".modal").show("fast");

    $(".accept").click(editUserAccept);
  }

  function editUserAccept() {
    let id = $(this).data("id");
    let required = true;
    let newObj = {
      id: id,
      name: $("#name").val(),
      phone: $("#phone").val(),
    };
    $(".modal .item input").map((i, item) => {
      if ($(item).val() == "") {
        required = false;
        $(item).css({ border: "1px solid #f71f1f" });
      } else {
        $(item).css({ border: "1px solid #4baf3d" });
      }
    });
    if (!required) {
      $(".error").remove();
      $(".modal form").prepend('<div class="error">Заповніть всі поля</div>');
    } else {
      getUsers = JSON.parse(localStorage.getItem("users"));
      getUsers.splice(id - 1, 1, newObj);
      usersJSON = JSON.stringify(getUsers);
      localStorage.setItem("users", usersJSON);
      $("#visitors-list").empty();
      showUsers(getUsers);
      $(".modal").hide("fast");
    }
  }

  // Sort

  $(".sort").click(() => {
    getUsers = JSON.parse(localStorage.getItem("users"));

    if ($("#sort").val() == 1) {
      getUsers.sort((a, b) => a.id - b.id);
    } else if ($("#sort").val() == 2) {
      getUsers.sort((a, b) => {
        let aName = a.name.toUpperCase();
        let bName = b.name.toUpperCase();
        let i = 0;
        if (aName > bName) {
          i = 1;
        } else if (aName < bName) {
          i = -1;
        }
        return i;
      });
    }

    usersJSON = JSON.stringify(getUsers);
    localStorage.setItem("users", usersJSON);
    $("#visitors-list").empty();
    showUsers(getUsers);
  });

  //Search

  $(".search").click(() => {
    getUsers = JSON.parse(localStorage.getItem("users"));

    let filterUsers = getUsers.filter((el) => {
      if ($("#search").val() == "") {
        return el;
      } else {
        return el.name.search($("#search").val()) != -1;
      }
    });
    $("#visitors-list").empty();
    showUsers(filterUsers);
  });
  //
  //
  //
});
