$(function () {
  let books = [
    {
      id: 1,
      nameBook: "PYTHON для дітей",
      author: "Джейсон Бриггс",
      year: "2017-06-01",
      namePublishing: "Видавництво Старого Лева",
      countPage: 400,
      countBook: 10,
    },
    {
      id: 2,
      nameBook: "Чистий код",
      author: "Роберт Мартин",
      year: "2019-05-01",
      namePublishing: "Фабула",
      countPage: 448,
      countBook: 15,
    },
    {
      id: 3,
      nameBook: "JavaScript. Карманный справочник",
      author: "Дэвид Флэнаган",
      year: "2020-05-01",
      namePublishing: "Диалектика",
      countPage: 320,
      countBook: 45,
    },
  ]

  let users = [
    {
      id: 1,
      name: "Yura",
      phone: 0964578963,
    },
    {
      id: 2,
      name: "Taras",
      phone: 0964530210,
    },
    {
      id: 3,
      name: "Oksana",
      phone: 0965524110,
    },
  ]

  let cards = [
    {
      id: 1,
      idUser: 1,
      idBook: 1,
      dateStart: "10/11/2020",
      dateEnd: "12/11/2020",
    },
    {
      id: 2,
      idUser: 1,
      idBook: 1,
      dateStart: "10/10/2020",
      dateEnd: "12/10/2020",
    },
    {
      id: 3,
      idUser: 2,
      idBook: 3,
      dateStart: "10/10/2020",
      dateEnd: "10/12/2021",
    },
  ]

  // local storage

  let booksJSON = JSON.stringify(books)
  let getBooks = JSON.parse(localStorage.getItem("books"))

  let usersJSON = JSON.stringify(users)
  let getUsers = JSON.parse(localStorage.getItem("users"))

  let cardsJSON = JSON.stringify(cards)
  let getCards = JSON.parse(localStorage.getItem("cards"))

  if (!getBooks || getBooks.length <= 0) {
    localStorage.setItem("books", booksJSON)
    getBooks = JSON.parse(localStorage.getItem("books"))
  }

  if (!getUsers || getUsers.length <= 0) {
    localStorage.setItem("users", usersJSON)
    getUsers = JSON.parse(localStorage.getItem("users"))
  }

  if (!getCards || getCards.length <= 0) {
    localStorage.setItem("cards", cardsJSON)
    getCards = JSON.parse(localStorage.getItem("cards"))
  }

  // show list
  showBooks(getBooks)
  function showBooks(books) {
    books.length == 0
      ? $("#books-list").empty()
      : books.map((item, i) => {
          $("#books-list").append(`
          <tr>
          <td>${item.id}</td>
          <td>${item.nameBook}</td>
          <td>${item.author}</td>
          <td>${item.year}</td>
          <td>${item.namePublishing}</td>
          <td>${item.countPage}</td>
          <td>${item.countBook}</td>
          <td class="edit" data-id="${i}"></td>
          <td class="delete" data-id="${i}"></td>
          </tr>
    `)
        })
    $(".delete").click(deleteBook)
    $(".table tr").on("click", ".edit", editBook)
    getBooks.map((item, i) => {
      if (item.countBook <= 0) {
        $("#books-list").children().eq(i).addClass("hiden")
        // getBooks.splice(i, 1);
        // booksJSON = JSON.stringify(getBooks);
        // localStorage.setItem("books", booksJSON);
      }
    })
  }
  // form
  function myForm(data) {
    let myClass = ""
    if (!data) {
      data = {
        nameBook: "",
        author: "",
        year: "",
        namePublishing: "",
        countPage: "",
        countBook: "",
      }
      myClass = "add"
    } else {
      myClass = "accept"
    }
    $(".modal form").empty()
    $(".modal form").append(`
      <div class="item">
        <label for="title">Title: </label>
        <input type="text" id="title" value="${data.nameBook}" />
      </div>
      <div class="item">
        <label for="author">Author: </label>
        <input type="text" id="author" value="${data.author}" />
      </div>
      <div class="item">
        <label for="year">Year: </label>
        <input type="date" id="year" value="${data.year}" />
      </div>
      <div class="item">
        <label for="publishing">Publishing house : </label>
        <input type="text" id="publishing" value="${data.namePublishing}" />
      </div>
      <div class="item">
        <label for="pages">Pages: </label>
        <input type="text" class="number"  id="pages" value="${data.countPage}" />
      </div>
       <div class="item">
        <label  for="quantity">Quantity: </label>
        <input type="text" class="number"  id="quantity" value="${data.countBook}" />
      </div>
      <div class="btn ${myClass}" data-id="${data.id}">${myClass}</div>
    `)

    $(".number").mask("999999")
  }

  //  Modal
  $(".close").click(() => {
    $(".modal").hide("fast")
  })

  // add book
  $(".new-book").click(() => {
    $(".modal h2").text("Add book")
    myForm()
    $(".modal").show("fast")
    $(".add").click(addBook)
  })

  function addBook() {
    let required = true
    let idBooks = getBooks.length + 1
    let newObj = {
      id: idBooks,
      nameBook: $("#title").val(),
      author: $("#author").val(),
      year: $("#year").val(),
      namePublishing: $("#publishing").val(),
      countPage: $("#pages").val(),
      countBook: $("#quantity").val(),
    }

    $(".books .modal .item input").map((i, item) => {
      if ($(item).val() == "") {
        required = false
        $(item).css({ border: "1px solid #f71f1f" })
      } else {
        $(item).css({ border: "1px solid #4baf3d" })
      }
    })
    if (!required) {
      $(".error").remove()
      $(".modal.addBook form").prepend('<div class="error">Заповніть всі поля</div>')
    } else {
      $(".error").remove()
      getBooks = JSON.parse(localStorage.getItem("books"))
      getBooks.push(newObj)
      booksJSON = JSON.stringify(getBooks)
      localStorage.setItem("books", booksJSON)
      $("#books-list").empty()
      showBooks(getBooks)
      $(".modal").hide("fast")
    }
  }

  // delete
  function deleteBook() {
    let id = $(this).data("id")
    getBooks = JSON.parse(localStorage.getItem("books"))
    getBooks.splice(id, 1)
    booksJSON = JSON.stringify(getBooks)
    localStorage.setItem("books", booksJSON)
    $("#books-list").empty()
    showBooks(getBooks)
  }
  //edit
  function editBook() {
    let id = $(this).data("id")
    let ourElement = getBooks[id]
    $(".modal h2").text("Edit form")
    myForm(ourElement)
    $(".modal").show("fast")

    $(".accept").click(editBookAccept)
  }

  function editBookAccept() {
    let id = $(this).data("id")
    let required = true
    let newObj = {
      id: id,
      nameBook: $("#title").val(),
      author: $("#author").val(),
      year: $("#year").val(),
      namePublishing: $("#publishing").val(),
      countPage: $("#pages").val(),
      countBook: $("#quantity").val(),
    }
    $(".books .modal .item input").map((i, item) => {
      if ($(item).val() == "") {
        required = false
        $(item).css({ border: "1px solid #f71f1f" })
      } else {
        $(item).css({ border: "1px solid #4baf3d" })
      }
    })
    if (!required) {
      $(".error").remove()
      $(".modal form").prepend('<div class="error">Заповніть всі поля</div>')
    } else {
      getBooks = JSON.parse(localStorage.getItem("books"))
      getBooks.splice(id - 1, 1, newObj)
      booksJSON = JSON.stringify(getBooks)
      localStorage.setItem("books", booksJSON)
      $("#books-list").empty()
      showBooks(getBooks)
      $(".modal").hide("fast")
    }
  }

  // SortBook

  $(".sort").click(() => {
    getBooks = JSON.parse(localStorage.getItem("books"))

    if ($("#sort").val() == 1) {
      getBooks.sort((a, b) => a.id - b.id)
    } else if ($("#sort").val() == 2) {
      getBooks.sort((a, b) => {
        let aName = a.nameBook.toUpperCase()
        let bName = b.nameBook.toUpperCase()
        let i = 0
        if (aName > bName) {
          i = 1
        } else if (aName < bName) {
          i = -1
        }
        return i
      })
    } else if ($("#sort").val() == 3) {
      getBooks.sort((a, b) => {
        let aName = a.author.toUpperCase()
        let bName = b.author.toUpperCase()
        let i = 0
        if (aName > bName) {
          i = 1
        } else if (aName < bName) {
          i = -1
        }
        return i
      })
    } else {
      getBooks.sort((a, b) => a.countBook - b.countBook)
    }

    booksJSON = JSON.stringify(getBooks)
    localStorage.setItem("books", booksJSON)
    $("#books-list").empty()
    showBooks(getBooks)
  })

  //Search

  $("#search").on("keyup", (e) => {
    e.preventDefault()
    getBooks = JSON.parse(localStorage.getItem("books"))

    let filterBooks = getBooks.filter((el) => {
      if ($("#search").val() == "") {
        return el
      } else {
        return el.nameBook.search($("#search").val()) != -1
      }
    })
    $("#books-list").empty()
    showBooks(filterBooks)
  })

  $(".search").click(() => {
    getBooks = JSON.parse(localStorage.getItem("books"))

    let filterBooks = getBooks.filter((el) => {
      if ($("#search").val() == "") {
        return el
      } else {
        return el.nameBook.search($("#search").val()) != -1
      }
    })
    $("#books-list").empty()
    showBooks(filterBooks)
  })
  //

  //
  //
})
