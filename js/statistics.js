$(function () {
  let getCards = JSON.parse(localStorage.getItem("cards"));
  let getUsers = JSON.parse(localStorage.getItem("users"));
  let getBooks = JSON.parse(localStorage.getItem("books"));
  let users = [];
  let books = [];

  getCards.map((item) => {
    let userObj = getUsers.find((el) => el.id == item.idUser);
    users.push(userObj.name);
    let booksObj = getBooks.find((el) => el.id == item.idBook);
    books.push(booksObj.nameBook);
  });

  let userTop = {};
  let userName = [];
  for (let i = 0; i < users.length; i++) {
    let count = userTop[users[i]];
    if (count) {
      userTop[users[i]] = count + 1;
    } else {
      userTop[users[i]] = 1;
    }
  }

  let booksTop = {};
  let booksName = [];
  for (let i = 0; i < books.length; i++) {
    let count = booksTop[books[i]];
    if (count) {
      booksTop[books[i]] = count + 1;
    } else {
      booksTop[books[i]] = 1;
    }
  }

  let sortableUser = Object.fromEntries(Object.entries(userTop).sort(([, a], [, b]) => b - a));
  let sortableBooks = Object.fromEntries(Object.entries(booksTop).sort(([, a], [, b]) => b - a));

  for (const key in sortableUser) {
    userName.push(key);
  }

  for (const key in sortableBooks) {
    booksName.push(key);
  }

  for (let k = booksName.length; k <= 5; k++) {
    booksName.push("");
  }
  for (let j = userName.length; j <= 5; j++) {
    userName.push("");
  }

  for (let i = 0; i < 5; i++) {
    $("#top-list").append(`<tr>
  <td>${booksName[i]}</td>
  <td>${userName[i]}</td>
  </tr>`);
  }
});
